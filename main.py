#!/usr/bin/env python3

from PySide2.QtGui import QGuiApplication
from PySide2.QtCore import SIGNAL, QObject, QAbstractListModel, Qt, Signal, Property
from PySide2.QtQml import QQmlApplicationEngine
import PySide2

class ExampleClass(QObject):

    def __init__(self):
        QObject.__init__(self)
        self._testprop = "Hello world"

    @Signal
    def testprop_changed(self):
        pass

    def readTestProp(self):
        return self._testprop

    def setTestProp(self, prop):
        if self._testprop != prop:
            self._testprop = prop
            self.testprop_changed.emit()

    testprop = Property(str, readTestProp, setTestProp, notify=testprop_changed)


if __name__ == "__main__":
    app = QGuiApplication()

    engine = QQmlApplicationEngine()

    PySide2.QtQml.qmlRegisterType(ExampleClass, "org.kde.test", 1, 0, "ExampleClass")

    context = engine.rootContext()
    engine.load("main.qml")

    if len(engine.rootObjects()) == 0:
        quit()

    app.exec_()
